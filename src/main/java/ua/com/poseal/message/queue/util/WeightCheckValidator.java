package ua.com.poseal.message.queue.util;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class WeightCheckValidator implements ConstraintValidator<WeightCheck, String> {

    @Override
    public void initialize(WeightCheck constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        String modified = s.replace("-", "");
        int control = Integer.parseInt(modified.substring(modified.length() - 1));
        String checkingNum = modified.substring(0, modified.length() - 1);
        String controlDigits = "731";
        int sum = 0;
        int i = 0;
        int j = 0;
        while (i < checkingNum.length()) {
            if (j >= controlDigits.length()) {
                j = 0;
            }
            sum = sum + Integer.parseInt(String.valueOf(checkingNum.charAt(i)))
                    * Integer.parseInt(String.valueOf(controlDigits.charAt(j)));
            j++;
            i++;
        }
        return sum % 10 == control;
    }
}
