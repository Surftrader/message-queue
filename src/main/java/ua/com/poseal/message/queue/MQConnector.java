package ua.com.poseal.message.queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import javax.jms.JMSException;

import java.io.IOException;
import java.util.Properties;

import static ua.com.poseal.message.queue.App.*;

public abstract class MQConnector {

    private static final int MAX_CONNECTIONS = 10;
    public final Properties properties;

    protected MQConnector(Properties properties) {
        this.properties = properties;
    }

    public void doConnect() {
        try {
            ActiveMQConnectionFactory connectionFactory =
                    createActiveMQConnectionFactory();
            PooledConnectionFactory pooledConnectionFactory =
                    createPooledConnectionFactory(connectionFactory);
            doJob(connectionFactory, pooledConnectionFactory);
            pooledConnectionFactory.stop();
            logger.debug("Stopping the PooledConnectionFactory");
        } catch (JMSException | IOException e) {
            logger.error("Error connection", e);
        } catch (InterruptedException e) {
            logger.error("Interrupted thread", e);
            Thread.currentThread().interrupt();
        }
    }

    protected abstract void doJob(
            ActiveMQConnectionFactory connectionFactory,
            PooledConnectionFactory pooledConnectionFactory) throws JMSException, IOException, InterruptedException;

    private PooledConnectionFactory createPooledConnectionFactory(ActiveMQConnectionFactory connectionFactory) {
        // Create a pooled connection factory.
        final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(MAX_CONNECTIONS);
        logger.info("PooledConnectionFactory was created with max connections={}", MAX_CONNECTIONS);
        return pooledConnectionFactory;
    }

    private ActiveMQConnectionFactory createActiveMQConnectionFactory() {
        // Create a connection factory.
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                properties.getProperty(USER_NAME),
                properties.getProperty(PASSWORD),
                properties.getProperty(LINK));
        connectionFactory.setTrustAllPackages(true);
        logger.info("ActiveMQConnectionFactory was created with name={}, link={}",
                properties.getProperty(USER_NAME), properties.getProperty(LINK));
        return connectionFactory;
    }
}
