package ua.com.poseal.message.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.com.poseal.message.queue.util.Loader;

import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.*;

public class App {
    public static final Logger logger = LoggerFactory.getLogger("LOGGER");
    public static final String LINK = "link";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String QUEUE_NAME = "queuename";
    public static final String POISON_PILL = "pill";
    private static final String DEFAULT_POJO_NUMBERS = "100000";
    public static final String GENERATION_POJO_NUMBERS = "N";
    public static final String PRODUCERS = "producers";
    protected static BlockingQueue<POJO> generatedQueue;
    protected static BlockingQueue<POJO> queueForWriting;

    public static void main(String[] args) {
        logger.info("Start program");
        try {
            queueForWriting = new LinkedBlockingDeque<>(5000);
            App app = new App();
            app.run();
        } catch (Exception e) {
            logger.error("Error!", e);
        }
        logger.info("Stop program");
    }

    private void run() {
        // Load properties
        Properties properties = new Loader().getFileProperties();
        String num = Optional.ofNullable(System.getProperty(GENERATION_POJO_NUMBERS))
                .orElse(DEFAULT_POJO_NUMBERS);
        properties.setProperty(GENERATION_POJO_NUMBERS, num);

        int producers = Integer.parseInt(properties.getProperty(PRODUCERS));
        POJOGenerator pojoGenerator = new POJOGenerator(properties);
        List<POJO> listPOJO = pojoGenerator.generateMessages();
        generatedQueue = new LinkedBlockingDeque<>(listPOJO);

        ExecutorService executorService = Executors.newFixedThreadPool(producers + 5);
        // Send messages via producer
        for (int i = 0; i < producers; i++) {
            executorService.execute(() -> new Producer(properties).doConnect());
        }
        // Receive messages via consumer
        executorService.execute(() -> new Consumer(properties).doConnect());

        // Write messages to files
        executorService.execute(() -> new CSVWriter(properties).write());

        closeExecutor(executorService);
    }

    private void closeExecutor(ExecutorService executorService) {
        executorService.shutdown();
        while (!executorService.isTerminated()) {
            Thread.onSpinWait();
        }
        try {
            // Wait for termination
            if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                // If the executor service does not terminate within the specified timeout,
                // you can call shutdownNow() to forcefully terminate the running tasks
                executorService.shutdownNow();

                // Wait again for termination
                if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                    logger.error("ExecutorService did not terminate");
                }
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
            logger.error("Forced termination of a thread", e);
        }
    }
}
