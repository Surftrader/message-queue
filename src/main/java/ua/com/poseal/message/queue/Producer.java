package ua.com.poseal.message.queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import javax.jms.*;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import static ua.com.poseal.message.queue.App.*;

public class Producer extends MQConnector {

    public Producer(Properties properties) {
        super(properties);
    }

    @Override
    protected void doJob(
            ActiveMQConnectionFactory connectionFactory,
            PooledConnectionFactory pooledConnectionFactory) throws JMSException, InterruptedException {
        // Establish a connection for the producer.
//        connectionFactory.setUseAsyncSend(true); or in property file add to url ?jms.useAsyncSend=true
        final Connection producerConnection = pooledConnectionFactory
                .createConnection();
        logger.debug("ProducerConnection was created");
        producerConnection.start();
        logger.debug("ProducerConnection was started");

        // Create a session.
        final Session producerSession = producerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);
        logger.debug("ProducerSession was created");

        // Create a queue
        final Destination producerDestination = producerSession
                .createQueue(properties.getProperty(QUEUE_NAME));
        logger.debug("ProducerDestination was created with queue name={}",
                properties.getProperty(QUEUE_NAME));

        // Create a producer from the session to the queue.
        final MessageProducer producer = producerSession
                .createProducer(producerDestination);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        logger.debug("MessageProducer was created");

        sendMessagesToQueue(producerSession, producer);
        logger.info("Messages sent.");

        cleanUpProducer(producerConnection, producerSession, producer);
    }

    private void sendMessagesToQueue(Session producerSession, MessageProducer producer)
            throws JMSException, InterruptedException {
        logger.info("Start sending messages.");
        long start = System.currentTimeMillis();

        AtomicInteger count = new AtomicInteger(1);
        ObjectMessage objectMessage = producerSession.createObjectMessage();
        while (true) {
            POJO pojo = generatedQueue.take();
            if (pojo.getName().equals(properties.getProperty(POISON_PILL))) {
                sendMessageToQueue(producer, count, objectMessage, pojo);
                break;
            }
            sendMessageToQueue(producer, count, objectMessage, pojo);
        }

        long end = System.currentTimeMillis();
        logger.info("{} messages was sent in {} milliseconds", count.get(), end - start);
        logger.info("RPS for Producer: {}", count.get() / ((end - start) / 1000.0));
    }

    private static void sendMessageToQueue(
            MessageProducer producer,
            AtomicInteger count,
            ObjectMessage objectMessage,
            POJO pojo) throws JMSException {
        objectMessage.setObject(pojo);
        producer.send(objectMessage);
        count.incrementAndGet();
    }

    private void cleanUpProducer(
            Connection producerConnection,
            Session producerSession,
            MessageProducer producer) throws JMSException {
        producer.close();
        logger.debug("Stopping MessageProducer");
        producerSession.close();
        logger.debug("Stopping Session");
        producerConnection.close();
        logger.debug("Stopping Connection");
    }
}
