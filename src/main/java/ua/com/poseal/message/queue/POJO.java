package ua.com.poseal.message.queue;

import jakarta.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Mod10Check;
import ua.com.poseal.message.queue.util.WeightCheck;

import java.io.Serializable;
import java.time.LocalDateTime;

public class POJO implements Serializable {
    @NotNull(message = "The name mustn't be null")
    @Length(min = 7, message = "Name must contain more than 7 letters")
    @Pattern(regexp = ".*[aA].*", message = "The name must contain the letter a")
    private final String name;
    @NotNull(message = "The name mustn't be null")
    @Length(min = 14, max = 14, message = "The EDDR number must be 14 characters long")
    @Mod10Check(message = "Check digit invalid")
    @WeightCheck(message = "Weight test 731 failed")
    private final String eddr;
    @NotNull(message = "The count mustn't be null")
    @Min(value = 10, message = "Count must be more than 9")
    private final int count;
    @NotNull(message = "The name mustn't be null")
    private final LocalDateTime createdAt;

    public POJO(String name) {
        this.name = name;
        this.eddr = null;
        this.count = 0;
        this.createdAt = null;
    }


    public POJO(String name, String eddr, int count, LocalDateTime createdAt) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public String getEddr() {
        return eddr;
    }

    public int getCount() {
        return count;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "POJO{" +
                "name='" + name + '\'' +
                ", eddr='" + eddr + '\'' +
                ", count=" + count +
                ", createdAt=" + createdAt +
                '}';
    }
}
