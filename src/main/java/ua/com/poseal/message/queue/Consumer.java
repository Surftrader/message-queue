package ua.com.poseal.message.queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import javax.jms.*;
import java.util.Properties;

import static ua.com.poseal.message.queue.App.*;

public class Consumer extends MQConnector {

    public Consumer(Properties properties) {
        super(properties);
    }

    @Override
    protected void doJob(
            ActiveMQConnectionFactory connectionFactory,
            PooledConnectionFactory pooledConnectionFactory) throws JMSException {
        // Establish a connection for the consumer.
        // Note: Consumers should not use PooledConnectionFactory.
        final Connection consumerConnection = connectionFactory
                .createConnection();
        logger.debug("ConsumerConnection was created");
        consumerConnection.start();
        logger.debug("ConsumerConnection was started");

        // Create a session.
        final Session consumerSession = consumerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);
        logger.debug("ConsumerSession was created");

        // Create a queue
        final Destination consumerDestination = consumerSession
                .createQueue(properties.getProperty(QUEUE_NAME));
        logger.debug("ConsumerDestination was created with queue name={}",
                properties.getProperty(QUEUE_NAME));

        // Create a message consumer from the session to the queue.
        final MessageConsumer consumer = consumerSession
                .createConsumer(consumerDestination);
        logger.debug("MessageConsumer was created");

        try {
            getMessagesFromQueue(consumer);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Forced termination of a thread", e);
        }
        logger.info("Messages received!");

        cleanUpConsumer(consumerConnection, consumerSession, consumer);
    }

    private void getMessagesFromQueue(MessageConsumer consumer) throws JMSException, InterruptedException {
        logger.info("Start reading messages.");
        int producers = Integer.parseInt(properties.getProperty(PRODUCERS));
        int counterPoisonPill = 0;
        int countMessage = 0;
        long start = System.currentTimeMillis();
        while (true) {
            ActiveMQObjectMessage objectMessage = (ActiveMQObjectMessage) consumer.receive(100);
            if (objectMessage != null) {
                POJO pojo = (POJO) objectMessage.getObject();
                if (pojo.getName().equals(properties.getProperty(POISON_PILL))) {

                    logger.info("producers:{}", producers);
                    counterPoisonPill++;
                    if (producers == counterPoisonPill) {
                        queueForWriting.put(pojo);
                        logger.info("Get poison pill -> {}. Loop exit.", properties.getProperty(POISON_PILL));
                        break;
                    }
                }
                queueForWriting.put(pojo);
                countMessage++;
            }
        }
        long finish = System.currentTimeMillis();
        logger.info("End reading messages.");
        logger.info("RPS for Consumer: {}", countMessage / ((finish - start) / 1000.0));
    }

    private void cleanUpConsumer(
            Connection consumerConnection,
            Session consumerSession,
            MessageConsumer consumer) throws JMSException {
        consumer.close();
        logger.debug("Stopping MessageConsumer");
        consumerSession.close();
        logger.debug("Stopping Session");
        consumerConnection.close();
        logger.debug("Stopping Connection");
    }
}
