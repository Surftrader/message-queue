package ua.com.poseal.message.queue.util;

import java.util.List;

public class Errors {
    private final List<String> errorList;

    public Errors(List<String> errors) {
        this.errorList = errors;
    }

    @Override
    public String toString() {
        return "\"\"\"{errors:" + errorList + "}\"\"\"";
    }
}
