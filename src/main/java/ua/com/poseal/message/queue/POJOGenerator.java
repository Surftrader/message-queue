package ua.com.poseal.message.queue;

import com.github.javafaker.Faker;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ua.com.poseal.message.queue.App.*;

public class POJOGenerator {
    public static final String INTERRUPT_TIME = "time";
    private static final String DATA_PATTERN = "yyyyMMdd";
    private static final int MAX_RANDOM_NUM = 99999;
    private static final int MIN_AGE = 18;
    private static final int MAX_AGE = 120;
    private final Faker faker;

    private final Properties properties;

    public POJOGenerator(Properties properties) {
        this.faker = new Faker();
        this.properties = properties;
    }

    public List<POJO> generateMessages() {
        logger.info("Start generating POJO");
        long start = System.currentTimeMillis();
        long end = start + Long.parseLong(properties.getProperty(INTERRUPT_TIME));
        long numbers = Long.parseLong(properties.getProperty(GENERATION_POJO_NUMBERS));
        AtomicInteger count = new AtomicInteger(1);

        List<POJO> messages = Stream.generate(() -> generateMessage(count.getAndIncrement()))
                .limit(numbers)
                .takeWhile(s -> System.currentTimeMillis() < end)
                .collect(Collectors.toList());

        int producers = Integer.parseInt(properties.getProperty(PRODUCERS));
        for (int i = 0; i < producers; i++) {
            messages.add(new POJO(properties.getProperty(POISON_PILL)));
        }
        logger.info(
                "{} messages were generated in {} sec",
                messages.size(), (System.currentTimeMillis() - start) / 1000);
        logger.info("End generating POJO");
        return messages;
    }

    private POJO generateMessage(Integer i) {
        return new POJO(
                generateName(),
                generateEDDR(),
                i,
                LocalDateTime.now());
    }

    private String generateName() {
        return faker.name().firstName();
    }

    private String generateEDDR() {
        return getBirthday() + "-" + getRandomDigits();
    }

    private String getRandomDigits() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(SecureRandom.getInstanceStrong().nextInt(MAX_RANDOM_NUM));
            int length = String.valueOf(MAX_RANDOM_NUM).length() - sb.length();
            for (int i = 0; i < length; i++) {
                sb.insert(0, i);
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("Error creation random number", e);
        }
        return sb.toString();
    }

    private String getBirthday() {
        Faker faker1 = new Faker();
        Date date = faker1.date().birthday(MIN_AGE, MAX_AGE);
        DateFormat dateFormat = new SimpleDateFormat(DATA_PATTERN);
        return dateFormat.format(date.getTime());
    }
}
