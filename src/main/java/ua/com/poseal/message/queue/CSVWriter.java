package ua.com.poseal.message.queue;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import ua.com.poseal.message.queue.util.Errors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static ua.com.poseal.message.queue.App.*;

public class CSVWriter {
    public static final String VALID_FILE = "validFile";
    public static final String INVALID_FILE = "errorFile";
    private final Validator validator;
    private final Properties properties;

    public CSVWriter(Properties properties) {
        this.properties = properties;
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public void write() {
        logger.info("Start writing messages");
        long start = System.currentTimeMillis();
        int counterMessage = 0;
        int counterPoisonPill = 0;
        int producers = Integer.parseInt(properties.getProperty(PRODUCERS));

        while (true) {
            try {
                POJO pojo = queueForWriting.take();
                if (pojo.getName().equals(properties.getProperty(POISON_PILL))) {
                    counterPoisonPill++;
                    if (counterPoisonPill == producers) {
                        break;
                    }
                } else {
                    Set<ConstraintViolation<POJO>> validate = validator.validate(pojo);
                    if (validate.isEmpty()) {
                        writeValidData(pojo);
                    } else {
                        List<String> errors = new ArrayList<>();
                        validate.forEach(error -> errors.add(error.getMessage()));
                        writeErrorData(pojo, errors);
                    }
                    counterMessage++;
                }
            } catch (InterruptedException e) {
                logger.error("Error writing message", e);
                Thread.currentThread().interrupt();
            }
        }
        logger.info("{} messages was written in {} milliseconds",
                counterMessage, System.currentTimeMillis() - start);
    }

    private void writeValidData(POJO pojo) {
        File file = new File(properties.getProperty(VALID_FILE));
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            if (file.length() == 0) {
                writer.write("name,count\n");
            }
            writer.write(pojo.getName() + "," + pojo.getCount() + "\n");
        } catch (IOException e) {
            logger.error("Error writing file", e);
        }
    }

    private void writeErrorData(POJO pojo, List<String> errors) {
        File file = new File(properties.getProperty(INVALID_FILE));
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            if (file.length() == 0) {
                writer.write("name,eddr,count,createdAt,errors\n");
            }
            writer.write(pojo.getName() + "," +
                    pojo.getEddr() + "," +
                    pojo.getCount() + "," +
                    pojo.getCreatedAt() + "," +
                    readErrorMessages(errors) + "\n");
        } catch (IOException e) {
            logger.error("Error writing file", e);
        }
    }

    private String readErrorMessages(List<String> errorList) {
        return new Errors(errorList).toString();
    }
}
