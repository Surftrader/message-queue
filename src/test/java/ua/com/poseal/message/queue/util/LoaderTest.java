package ua.com.poseal.message.queue.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static ua.com.poseal.message.queue.App.*;
import static ua.com.poseal.message.queue.CSVWriter.INVALID_FILE;
import static ua.com.poseal.message.queue.CSVWriter.VALID_FILE;

class LoaderTest {
    Loader loader;

    @BeforeEach
    void init() {
        loader = new Loader();
    }

    @Test
    void testGetFileProperties() {
        Properties properties = loader.getFileProperties();

        assertNotNull(properties);
        assertEquals("AWSBroker", properties.getProperty(USER_NAME));
        assertEquals("MyQueue", properties.getProperty(QUEUE_NAME));
        assertEquals("valid_pojo.csv", properties.getProperty(VALID_FILE));
        assertEquals("error_pojo.csv", properties.getProperty(INVALID_FILE));

    }
}