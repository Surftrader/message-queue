package ua.com.poseal.message.queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static ua.com.poseal.message.queue.App.*;
import static ua.com.poseal.message.queue.POJOGenerator.INTERRUPT_TIME;

class POJOGeneratorTest {

    Properties properties;
    POJOGenerator pojoGenerator;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty(INTERRUPT_TIME, "10000");
        properties.setProperty(GENERATION_POJO_NUMBERS, "1000");
        properties.setProperty(PRODUCERS, "3");
        properties.setProperty(POISON_PILL, "END_OF MESSAGES");
        pojoGenerator = new POJOGenerator(properties);
    }

    @Test
    void generateMessages() {
        List<POJO> pojoList = pojoGenerator.generateMessages();
        assertNotNull(pojoList);
    }

    @Test
    void generateMessage() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = POJOGenerator.class.getDeclaredMethod("generateMessage", Integer.class);
        method.setAccessible(true);
        int expected = 5;
        POJO pojo = (POJO) method.invoke(pojoGenerator, expected);

        assertNotNull(pojo.getEddr());
        assertNotNull(pojo.getName());
        assertEquals(14, pojo.getEddr().length());
        assertEquals(expected, pojo.getCount());
        assertNotNull(pojo.getCreatedAt());

    }
}
