package ua.com.poseal.message.queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static ua.com.poseal.message.queue.App.*;

class ConsumerTest {
    Properties properties;
    MQConnector mqConnector;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty(USER_NAME, "AWSBroker");
        properties.setProperty(PASSWORD, "1234AWSBroker5678");
        properties.setProperty(LINK, "ssl://b-e422aef7-c615-441c-98d1-b2b35b61dcd6-1.mq.eu-west-3.amazonaws.com:61617");
        mqConnector = new Consumer(properties);
    }

    @Test
    void testCreatePooledConnectionFactory()
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        // Setup test data
        ActiveMQConnectionFactory connectionFactoryMock = mock(ActiveMQConnectionFactory.class);
        PooledConnectionFactory pooledConnectionFactoryMock = mock(PooledConnectionFactory.class);
        int maxConnections = 10;
        // Setting expectations and checking method calls
        when(pooledConnectionFactoryMock.getConnectionFactory()).thenReturn(connectionFactoryMock);

        Method method = MQConnector.class.getDeclaredMethod(
                "createPooledConnectionFactory", ActiveMQConnectionFactory.class);
        method.setAccessible(true);
        PooledConnectionFactory result = (PooledConnectionFactory) method.invoke(mqConnector, connectionFactoryMock);
        int actual = result.getMaxConnections();
        assertEquals(maxConnections, actual);
    }

    @Test
    void testCreateActiveMQConnectionFactory()
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = MQConnector.class
                .getDeclaredMethod("createActiveMQConnectionFactory", (Class<?>[]) null);
        method.setAccessible(true);
        ActiveMQConnectionFactory result = (ActiveMQConnectionFactory) method.invoke(mqConnector);
        assertNotNull(result);
    }
}
