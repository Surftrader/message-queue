package ua.com.poseal.message.queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class CSVWriterTest {

    Properties properties;
    CSVWriter writer;
    List<String> errors;

    @BeforeEach
    void init() {
        properties = new Properties();
        writer = new CSVWriter(properties);
        errors = new ArrayList<>();
    }

    @Test
    void testReadErrorMessages() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = CSVWriter.class.getDeclaredMethod("readErrorMessages", List.class);
        method.setAccessible(true);
        String actual = (String) method.invoke(writer, errors);
        assertNotNull(actual);
    }
}